/**
 * View Models used by Spring MVC REST controllers.
 */
package com.sepayment.payment.web.rest.vm;
