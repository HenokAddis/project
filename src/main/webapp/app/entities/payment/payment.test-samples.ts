import { IPayment, Payment } from './payment.model';

export const sampleWithRequiredData: IPayment = {
  id: 47537,
  cik: 'connect RSS eco-centric',
  ccc: 'functionalities Account',
  paymentAmount: 'Cotton',
  name: 'microchip strategic methodical',
  email: 'Margarete_Breitenberg90@hotmail.com',
  phone: '1-679-324-8964 x608',
  approvalStatus: 'Kids Corporate',
};

export const sampleWithPartialData: IPayment = {
  id: 75621,
  cik: 'Innovative Cuban',
  ccc: 'Money Soft empower',
  paymentAmount: 'Turkish Small',
  name: 'Dalasi turquoise Card',
  email: 'Schuyler_Reichert16@gmail.com',
  phone: '(930) 764-4582',
  approvalStatus: 'District',
};

export const sampleWithFullData: IPayment = {
  id: 3285,
  cik: 'RAM',
  ccc: 'Developer',
  paymentAmount: 'portal Group FTP',
  name: 'distributed aggregate',
  email: 'Abigail.Runolfsson24@yahoo.com',
  phone: '401-507-6364 x9716',
  approvalStatus: 'Pants Estate',
};

export const sampleWithNewData: Payment = {
  cik: 'Markets',
  ccc: 'Table Bacon',
  paymentAmount: 'Salad',
  name: 'innovate array Plaza',
  email: 'Ruben.Smitham@hotmail.com',
  phone: '562-669-4520',
  approvalStatus: 'hacking',
  id: 1,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
