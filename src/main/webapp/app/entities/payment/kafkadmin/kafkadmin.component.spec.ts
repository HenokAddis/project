import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { expect } from '@jest/globals';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PaymentService } from '../service/payment.service';
import { KafkadminComponent } from './kafkadmin.component';

describe('KafkadminComponent', () => {
  let comp: KafkadminComponent;
  let fixture: ComponentFixture<KafkadminComponent>;
  let service: PaymentService;
  let mockActiveModal: NgbActiveModal;
  const payments: any = ['1', '00001234', 'henok1@AAU', '12000', 'henok', 'henok@gmail.com', '0987654321', 'approve'];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [KafkadminComponent],
      imports: [HttpClientTestingModule],
      providers: [NgbActiveModal],
    })
      .overrideTemplate(KafkadminComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(KafkadminComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(PaymentService);
    mockActiveModal = TestBed.inject(NgbActiveModal);
  });

  describe('', () => {
    it('should decline', () => {
      comp.decline(payments);
      expect(comp).toBeTruthy();
    });

    it('should load kafka admin', () => {
      comp.kafkaAdmin();
      expect(comp).toBeTruthy();
    });

    it('should load all', () => {
      comp.loadAll();
      expect(comp).toBeTruthy();
    });

    it('should load openError', () => {
      comp.openError();
      expect(comp).toBeTruthy();
    });

    it('should load openSuccess', () => {
      comp.openSuccess();
      expect(comp).toBeTruthy();
    });

    it('should approve', () => {
      comp.save(payments);
      expect(comp).toBeTruthy();
    });

    it('should load ngOnInit', () => {
      comp.ngOnInit();
      expect(comp).toBeTruthy();
    });
  });
});
