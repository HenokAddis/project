import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { expect } from '@jest/globals';
import { PaymentDetailComponent } from './payment-detail.component';
import { PaymentService } from '../service/payment.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('Payment Management Detail Component', () => {
  let comp: PaymentDetailComponent;
  let fixture: ComponentFixture<PaymentDetailComponent>;
  let service: PaymentService;
  let mockActiveModal: NgbActiveModal;
  // const payments:any = ['1','00001234','henok1@AAU','12000','henok','henok@gmail.com','0987654321','approve'];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PaymentDetailComponent],
      imports: [HttpClientTestingModule],
      providers: [NgbActiveModal],
    }).compileComponents();
    fixture = TestBed.createComponent(PaymentDetailComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(PaymentService);
    mockActiveModal = TestBed.inject(NgbActiveModal);
  });

  describe('Should load DetaileComponent', () => {
    it('should approve', () => {
      comp.save();
      expect(comp).toBeTruthy();
    });

    it('should load ngOnInit', () => {
      comp.ngOnInit();
      expect(comp).toBeTruthy();
    });

    it('should approve', () => {
      comp.previousState();
      expect(comp).toBeTruthy();
    });

    it('should load ngOnInit', () => {
      comp.loadAll();
      expect(comp).toBeTruthy();
    });
  });
});
