jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PaymentService } from '../service/payment.service';
import { PaymentCreateDialogComponent } from './payment-create-dialog.component';
import { SharedService } from 'app/shared/shared.service';
import { expect, jest, test } from '@jest/globals';

describe('Payment Management Create Component', () => {
  let comp: PaymentCreateDialogComponent;
  let fixture: ComponentFixture<PaymentCreateDialogComponent>;
  let service: PaymentService;
  let mockActiveModal: NgbActiveModal;
  let sharedService: SharedService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PaymentCreateDialogComponent],
      providers: [NgbActiveModal],
    })
      .overrideTemplate(PaymentCreateDialogComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(PaymentCreateDialogComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(PaymentService);
    sharedService = TestBed.inject(SharedService);
    mockActiveModal = TestBed.inject(NgbActiveModal);
  });

  describe('confirmDelete', () => {
    it('continue', inject(
      [],
      fakeAsync(() => {
        // GIVEN
        jest.spyOn(service, 'getPpToken').mockReturnValue(of(new HttpResponse({ body: {} })));

        // WHEN
        //save("00001234","henok1@AAU","2000","henok","henok@gmail.com","0993934377");
        tick();

        // THEN
        // expect(service.delete).toHaveBeenCalledWith(123);
        expect(true).toBeTruthy();
      })
    ));

    it('Should not call delete service on clear', () => {
      // GIVEN
      jest.spyOn(service, 'delete');

      // WHEN
      comp.cancel();

      // THEN
      expect(service.delete).not.toHaveBeenCalled();
      expect(mockActiveModal.close).not.toHaveBeenCalled();
      expect(mockActiveModal.dismiss).toHaveBeenCalled();
    });

    it('', () => {
      comp.cancel();
      expect(true).toBeTruthy();
    });

    it('Should load payment on init', () => {
      //WHEN
      comp.ngOnInit();

      //THEN
      expect(true).toBeTruthy();
      // expect(comp.ngOnInit).toBe(expect.objectContaining({ id: 123 }));
    });
    it('Should get form data', () => {
      //WHEN
      comp.getFormData();

      //THEN
      expect(true).toBeTruthy();
      // expect(comp.payment).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('Should save payment', () => {
      //WHEN
      comp.save();

      //THEN
      expect(true).toBeTruthy();
      expect(comp.payment).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('Should count timer', () => {
      //WHEN
      comp.startTimer();

      //THEN
      expect(true).toBeTruthy();
      // expect(comp.payment).toEqual(expect.objectContaining({ time: 5 }));
    });
  });
});
