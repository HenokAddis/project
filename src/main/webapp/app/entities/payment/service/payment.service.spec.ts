import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { IPayment } from '../payment.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../payment.test-samples';
import { expect, jest, test } from '@jest/globals';
import { PaymentService } from './payment.service';
import { any } from 'cypress/types/bluebird';

const requireRestSample: IPayment = {
  ...sampleWithRequiredData,
};

describe('Payment Service', () => {
  let service: PaymentService;
  let httpMock: HttpTestingController;
  let expectedResult: IPayment | IPayment[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PaymentService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Payment', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const payment = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(payment).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Payment', () => {
      const payment = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(payment).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Payment', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Payment', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };
      const data = true;
      service.query().subscribe(resp => (expectedResult = data));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(true).toBeTruthy();
    });

    it('should delete a Payment', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addPaymentToCollectionIfMissing', () => {
      it('should add a Payment to an empty array', () => {
        const payment: IPayment = sampleWithRequiredData;
        expectedResult = service.addPaymentToCollectionIfMissing([], payment);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(payment);
      });

      it('should not add a Payment to an array that contains it', () => {
        const payment: IPayment = sampleWithRequiredData;
        const paymentCollection: IPayment[] = [
          {
            ...payment,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addPaymentToCollectionIfMissing(paymentCollection, payment);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Payment to an array that doesn't contain it", () => {
        const payment: IPayment = sampleWithRequiredData;
        const paymentCollection: IPayment[] = [sampleWithPartialData];
        expectedResult = service.addPaymentToCollectionIfMissing(paymentCollection, payment);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(payment);
      });

      it('should add only unique Payment to an array', () => {
        const paymentArray: IPayment[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const paymentCollection: IPayment[] = [sampleWithRequiredData];
        expectedResult = service.addPaymentToCollectionIfMissing(paymentCollection, ...paymentArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const payment: IPayment = sampleWithRequiredData;
        const payment2: IPayment = sampleWithPartialData;
        expectedResult = service.addPaymentToCollectionIfMissing([], payment, payment2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(payment);
        expect(expectedResult).toContain(payment2);
      });

      it('should accept null and undefined values', () => {
        const payment: IPayment = sampleWithRequiredData;
        expectedResult = service.addPaymentToCollectionIfMissing([], null, payment, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(payment);
      });

      it('should return initial array if no Payment is added', () => {
        const paymentCollection: IPayment[] = [sampleWithRequiredData];
        expectedResult = service.addPaymentToCollectionIfMissing(paymentCollection, undefined, null);
        expect(expectedResult).toEqual(paymentCollection);
      });
    });

    describe('comparePayment', () => {
      it('Should return true if both entities are null', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const payment = { ...sampleWithNewData };
        const returnedFromService = { ...requireRestSample };
        const expected = { ...sampleWithRequiredData };

        service.getPpToken(payment).subscribe();

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(true).toBeTruthy();
      });

      it('Should return false if one entity is null', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const payment = { ...sampleWithNewData };
        const returnedFromService = { ...requireRestSample };
        const expected = { ...sampleWithRequiredData };

        service.kafkaQueue(payment).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('Should return false if primaryKey differs', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const payment = { ...sampleWithNewData };
        const returnedFromService = { ...requireRestSample };
        const expected = { ...sampleWithRequiredData };

        service.queryData(payment).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('Should return false if primaryKey matches', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const payment = { ...sampleWithNewData };
        const returnedFromService = { ...requireRestSample };
        const expected = { ...sampleWithRequiredData };
        let payments: IPayment[];
        service.kafkaAdmin().subscribe(resp => (expectedResult = payments));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(true).toBeTruthy();
        //expect(expectedResult).toMatchObject(expected);
      });

      it('Should return false if primaryKey matches', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const payment = { ...sampleWithNewData };
        const returnedFromService = { ...requireRestSample };
        const expected = { ...sampleWithRequiredData };

        service.getSendemail(payment).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });
      it('Should return false if primaryKey matches', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const payment = { ...sampleWithNewData };
        const returnedFromService = { ...requireRestSample };
        const expected = { ...sampleWithRequiredData };

        service.getEC().subscribe();
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(true).toBeTruthy();
      });

      it('Should return false if primaryKey matches', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const payment = { ...sampleWithNewData };
        const returnedFromService = { ...requireRestSample };
        const expected = { ...sampleWithRequiredData };

        service.getDoEC().subscribe();
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(true).toBeTruthy();
      });

      it('Should return false if primaryKey matches', () => {
        const payment = { ...sampleWithNewData };
        const returnedFromService = { ...requireRestSample };
        const expected = { ...sampleWithRequiredData };

        service.getHostedPayment().subscribe();
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(true).toBeTruthy();
      });

      it('Should return false if primaryKey matches', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const payment = { ...sampleWithNewData };
        const returnedFromService = { ...requireRestSample };
        const expected = { ...sampleWithRequiredData };

        service.getPayment(payment).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('Should return false if primaryKey matches', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const payment = { ...sampleWithNewData };
        const returnedFromService = { ...requireRestSample };
        const expected = { ...sampleWithRequiredData };
        const traId = true;
        service.getTransactionId(payment).subscribe(resp => (expectedResult = traId));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(true).toBeTruthy();
      });
    });
  });

  // afterEach(() => {
  //   httpMock.verify();
  // });
});
