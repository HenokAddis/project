import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, finalize } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IPayment } from '../payment.model';
import { PaymentService } from '../service/payment.service';
import { PaymentDeleteDialogComponent } from '../delete/payment-delete-dialog.component';
import { SortService } from 'app/shared/sort/sort.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'jhi-payment',
  templateUrl: './payment.component.html',
})
export class PaymentComponent implements OnInit {
  payments?: IPayment[];
  isLoading = false;
  pay: any;
  payment: any;
  paymentAmounts: any;
  predicate: any;
  ascending: any;
  constructor(
    protected paymentService: PaymentService,
    protected activatedRoute: ActivatedRoute,
    public router: Router,
    protected sortService: SortService,
    protected modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.pay = sessionStorage.getItem('payment');
    this.payment = JSON.parse(this.pay);
    this.loadAll();
    this.subscribeToSaveResponse(this.paymentService.getSendemail(this.payment));
  }
  loadAll(): void {
    this.isLoading = true;
    this.pay = sessionStorage.getItem('payment');
    this.paymentAmounts = JSON.parse(this.pay).paymentAmount;

    this.paymentService.query().subscribe({
      next: (res: HttpResponse<string>) => {
        this.isLoading = false;
        this.pay = res.body ?? '';

        this.payment = JSON.parse(JSON.stringify(this.pay));
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  delete(payment: IPayment): void {
    const modalRef = this.modalService.open(PaymentDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.payment = payment;
  }
  onSaveFinalize(): void {}
  onSaveSuccess(): void {}
  onSaveError(): void {}
  protected subscribeToSaveResponse(result: Observable<HttpResponse<any>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: (res: any) => {
        this.onSaveSuccess();
      },
      error: () => this.onSaveError(),
    });
  }
}
