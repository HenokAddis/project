import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { expect } from '@jest/globals';
import { PaymentService } from '../service/payment.service';
import { PaymentComponent } from './payment.component';

describe('Payment Management Component', () => {
  let comp: PaymentComponent;
  let fixture: ComponentFixture<PaymentComponent>;
  let service: PaymentService;
  const payments: any = ['1', '00001234', 'henok1@AAU', '12000', 'henok', 'henok@gmail.com', '0987654321', 'approve'];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'payment', component: PaymentComponent }]), HttpClientTestingModule],
      declarations: [PaymentComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(PaymentComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PaymentComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(PaymentService);
  });

  describe('trackId', () => {
    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.payments?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
    it('should load all', () => {
      comp.loadAll();
      expect(comp).toBeTruthy();
    });

    it('should approve', () => {
      comp.delete(payments);
      expect(comp).toBeTruthy();
    });

    it('should load onSaveError', () => {
      comp.onSaveError();
      expect(comp).toBeTruthy();
    });

    it('should load onSaveFinalize', () => {
      comp.onSaveFinalize();
      expect(comp).toBeTruthy();
    });

    it('should load onSaveSuccess', () => {
      comp.onSaveSuccess();
      expect(comp).toBeTruthy();
    });
  });
});
