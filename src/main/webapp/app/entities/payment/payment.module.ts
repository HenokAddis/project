import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { PaymentComponent } from './list/payment.component';
import { PaymentDetailComponent } from './detail/payment-detail.component';
import { PaymentUpdateComponent } from './update/payment-update.component';
import { PaymentDeleteDialogComponent } from './delete/payment-delete-dialog.component';
import { PaymentRoutingModule } from './route/payment-routing.module';
import { PaymentCreateDialogComponent } from './create/payment-create-dialog.component';
import { KafkadminComponent } from './kafkadmin/kafkadmin.component';

@NgModule({
  imports: [SharedModule, PaymentRoutingModule],
  declarations: [
    PaymentCreateDialogComponent,
    KafkadminComponent,
    PaymentComponent,
    PaymentDetailComponent,
    PaymentUpdateComponent,
    PaymentDeleteDialogComponent,
  ],
})
export class PaymentModule {}
