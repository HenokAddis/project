import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentComponent } from '../list/payment.component';
import { PaymentDetailComponent } from '../detail/payment-detail.component';
import { PaymentUpdateComponent } from '../update/payment-update.component';
import { PaymentRoutingResolveService } from './payment-routing-resolve.service';
import { KafkadminComponent } from '../kafkadmin/kafkadmin.component';
import { PaymentCreateDialogComponent } from '../create/payment-create-dialog.component';
const paymentRoute: Routes = [
  {
    path: 'list',
    component: PaymentComponent,
  },

  {
    path: 'kafka',
    component: KafkadminComponent,
  },

  {
    path: 'view',
    component: PaymentDetailComponent,
    resolve: {
      payment: PaymentRoutingResolveService,
    },
  },
  {
    path: 'delete',
    component: PaymentCreateDialogComponent,
    resolve: {
      payment: PaymentRoutingResolveService,
    },
  },

  {
    path: 'new',
    component: PaymentUpdateComponent,
    resolve: {
      payment: PaymentRoutingResolveService,
    },
  },
  {
    path: '',
    component: PaymentUpdateComponent,
    resolve: {
      payment: PaymentRoutingResolveService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(paymentRoute)],
  exports: [RouterModule],
})
export class PaymentRoutingModule {}
